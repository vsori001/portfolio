  <!-- Preloader -->
    <div id="preloader">
      <div class="loader"></div>
    </div><!--preloader end-->

    <main class="page-container">

        <!-- Profile Page -->
            <section class="single-page">

                <!-- Profile Page Title-->
                <header class="page-title">
                    <span><i class="fa fa-user fa-3x"></i></span>
                    <h2>Profile</h2>
                    <p class="menu-desc">A Little Bit About Me...</p>
                </header>
                <!-- .page-title -->

		    <!-- Profile Page Contents-->
                <article class="page-info container-fluid">
                 <!-- about me -->
                    <section class="row about-me">
                        <div class="col-sm-5 col-sm-offset-1 profile-image">
                            <div class="pp-container">
                                <img src="<?php echo get_bloginfo( 'template_directory' );?>/img/avatar.png" alt="">
                            </div>
                            <h2><span>Vanessa </span><span>Maciel</span></h2>
                            <h3>Front-End Web Developer</h3>

                        </div>
                        <!--.Profile image end-->

                        <!-- bio-->
                        <div class="col-sm-6 bio">
                            <div class="bio-inner">
                                <div class="col-md-10">
                                    <h3>I am a hard-working, team player, get things done type of web developer!</h3>
                                    <p>The moment I learned how to create a website I was hooked. I loved everything about it! The way it represents an idea. The intricacies of styling, and placement, and overall design. After college, I taught myself everything I could about what it took to develop things for the web.</p>
                                    <p>I have a passion for this and I put my all into what I do. I know this field is ever changing. I do my best to stay up to date with best practices and some new features that aide in making web developing better and easier.</p>

                                    <div class="buttons">
                                        <a href="mailto:veesoria@gmail.com" class="btn hire-me">Hire Me</a>
                                        <a href="resume.pdf" target="_blank" class="btn download-resume">Download Resume</a>
                                    </div>

                                </div>
                                </div>
                        </div>
                        <!-- .bio End -->
                    </section>
                    <!-- .about-me end-->

                    <!-- services-->
                    <section class="row services">
                        <div class="over-div"></div>
                        <div class="sec-divider"><i class="fa fa-briefcase" aria-hidden="true" style="font-size:30px;"></i></div>
                        <h2 class="section-title">What I can offer</h2>

                        <div class="container service-list">

                            <div class="row">
                                <div class="col-sm-6 col-md-6 service">
                                    <span><i class="fa fa-envelope-o fa-3x"></i></span>
                                    <h3>Emails</h3>
                                    <p>Need an email? You got it! I can provide email templates, code, and ideas that work on your phone and your desktop.</p>
                                </div>

                                <div class="col-sm-6 col-md-6 service">
                                    <span><i class="fa fa-code fa-3x"></i></span>
                                    <h3>Front-End Web Development</h3>
                                    <p>Familiar with Wordpress and similar CMS tools. Proficient with HTML, CSS, JQuery, and PHP to get the look you want.</p>
                                </div>

                                <!-- <div class="col-sm-6 col-md-3 service">
                                    <span><i class="fa fa-android fa-3x"></i></span>
                                    <h3>Android App</h3>
                                    <p>Consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreeet doloremagna erat. </p>
                                </div> -->

                                <!-- <div class="col-sm-6 col-md-3 service">
                                    <span><i class="fa fa-wordpress fa-3x"></i></span>
                                    <h3>WordPress</h3>
                                    <p>Consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreeet doloremagna erat. </p>
                                </div> -->
                            </div> <!-- row of services -->
                        </div> <!-- service list-->

                    </section>
                    <!-- #Services end-->

                    <section class="row fun-facts">
                        <ul>
                            <li>
                                <h3><i class="fa fa-cutlery"></i></h3>
                                <h5>Cook</h5>
                            </li>
                            <li>
                                <h3><i class="fa fa-heartbeat"></i></h3>
                                <h5>Gym Goer</h5>
                            </li>
                            <li>
                                <h3>300+</h3>
                                <h5>Coffee Cups</h5>
                            </li>
                        </ul>
                    </section>
                    <!-- Fun facts end-->

                    <footer class="row footer">
                        <a href="http://vanessaannemaciel.com">Vanessa Maciel</a>
                    </footer>
                    <!--footer end-->

                </article>
                <!-- .page-info -->
            </section> <!-- Single Page end -->


            <!-- Resume Page -->

            <section class="single-page">
                <header class="page-title">
                    <span><i class="fa fa-file-text fa-3x"></i></span>
                    <h2>Resume</h2>
                    <p class="menu-desc">My Work History...</p>
                </header>
                <!-- .page-title -->

			<!-- Resume Page Contents-->
                <article class="page-info container-fluid">
                    <section class="row resume">
                        <div class="over-div"></div>
                        <div class="container">
                            <div class="col-xs-6 education">
                                <h2>- Education -</h2>

                                <ul class="edu-list">
                                    <li>
                                        <h3>University of California, Riverside</h3>
                                        <h4>Bachelor of Science, Computer Science</h4>
                                        <span>2006-2011</span>
                                        <p>Studied object oriented programming mainly with C++. Also studied topics on software engineering, UNIX systems administration, and embedded systems.</p>
                                    </li>
                                    <li>
                                        <h3>UCLA Design Communication Arts Certification Program</h3>
                                        <h4>2011-2012</h4>
                                        <p>Started taking Design courses to get a better perspective of design and the tools needed to execute it.</p>
                                    </li>
                                </ul>
                            </div>

                            <div class="col-xs-6 employment">
                                <h2>- Employment -</h2>

                                <ul class="employment-list">
                                    <li>
                                        <h3>Grizzard Communications Inc.</h3>
                                        <h4>Front-End Web Developer</h4>
                                        <span>May 2012 - Present</span>
                                        <p>Responsible for creating, maintaining, and providing support to client websites. Assisted in the creation of websites, social media sharing tools, and responsive donation forms and landing pages. HTML, CSS, Javascript, JQuery, PHP, LESS are some languages used daily. CMS experience in Wordpress as well as GIT to push updates. Familiar with marketing tools like Blackbaud and 2dialog to help assist in email deployments. Currently creating an automating tool using GULP to code emails quickly. Created email templates specific to each clients brand.</p>
                                    </li>
                                    <li>
                                        <h3>Trailer Park Publishing</h3>
                                        <h4>Front-End Web Developer</h4>
                                        <span>Oct 2011 - Apr 2012</span>
                                        <p>Created eBooks using XHTML, HTML5, CSS, and CSS3. Worked solo as well as in teams to complete projects. Quality tested projects on tablets and mobile devices. Paid close attention to detail for each project following kerning, positioning, and image styling as specified by the client.</p>
                                    </li>
                                    <li>
                                        <h3>Sacred Heart Sisters</h3>
                                        <h4>Freelance Front-End Web Developer</h4>
                                        <span>Aug 2011 - Jun 2015</span>
                                        <p>Helped reorganize their website information. Regularly updated their homepage with upcoming events, relevant links, and images.</p>
                                    </li>
                                    <li>
                                        <h3>Holy Spirit Retreat Center</h3>
                                        <h4>Web Developer/Designer</h4>
                                        <span>Jun 2010 - Oct 2011</span>
                                        <p>Designed, updated, and managed the website using HTML, CSS, Javascript and PHP. Provided technical support to the staff regarding computer, software, and printer issues. Answered phones and gave information to potential clients of the retreat center. Performed office tasks requiring the use of Microsoft Word and Excel to create flyers and mailing lists.</p>
                                    </li>
                                </ul>
                            </div>
                        </div> <!-- .container-->
                    </section>
                    <!-- Resume section end -->

                    <section class="row skills">
                        <div class="over-div"></div>
                        <div class="sec-divider"></div>
                        <h2 class="section-title">My Key Skills</h2>

                            <div class='skill-container container'>
                                <div class='row'>
                                    <div class='col-xs-6 col-sm-4 skill'>
                                        <figure>
                                            <canvas height='150' id='html_css' width='150'></canvas>
                                            <figcaption>
                                                HTML &amp; CSS
                                            </figcaption>
                                        </figure>
                                    </div>
                                    <div class='col-xs-6 col-sm-4 skill'>
                                        <figure>
                                            <canvas height='150' id='less' width='150'></canvas>
                                            <figcaption>
                                                LESS
                                            </figcaption>
                                        </figure>
                                    </div>
                                    <div class='col-xs-6 col-sm-4 skill'>
                                        <figure>
                                            <canvas height='150' id='jquery' width='150'></canvas>
                                            <figcaption>
                                                jQuery
                                            </figcaption>
                                        </figure>
                                    </div>

                                    <div class='col-xs-6 col-sm-4 skill'>
                                        <figure>
                                            <canvas height='150' id='php' width='150'></canvas>
                                            <figcaption>
                                                PHP
                                            </figcaption>
                                        </figure>
                                    </div>
                                    <div class='col-xs-6 col-sm-4 skill'>
                                        <figure>
                                            <canvas height='150' id='git' width='150'></canvas>
                                            <figcaption>
                                                GIT
                                            </figcaption>
                                        </figure>
                                    </div>
                                    <div class='col-xs-6 col-sm-4 skill'>
                                        <figure>
                                            <canvas height='150' id='gulp' width='150'></canvas>
                                            <figcaption>
                                                GULP
                                            </figcaption>
                                        </figure>
                                    </div>
                                </div> <!-- row end-->
                            </div> <!-- Skill container -->
                    </section>
                    <!-- Skills Section end -->

                    <section class="row recognition">
                        <div class="col-sm-6 rec-desc">
                            <div class="rec-inner">
                                <h2>Other Projects</h2>
                                <p></p>
                            </div>
                        </div>
                        <div class="col-sm-6 rec-list">
                            <ul>
                                <li>
                                    <span class="fa fa-trophy fa-5x pull-left fa-fw"></span>
                                    <h3>Pitaya Plus</h3>
                                    <h5>Contact Form</h5>
                                    <h4></h4>
                                    <p>Took their current contact form on shopify, modified items the client wanted modified and also made this form repsponsive.</p>
                                </li>

                                <li>
                                    <span class="fa fa-trophy fa-5x pull-left fa-fw"></span>
                                    <h3>Pardot</h3>
                                    <h5>DealerMatch Sign Up Forms</h5>
                                    <h4></h4>
                                    <p>Took their current sign up form and made it responsive</p>
                                </li>

                                <li>
                                    <span class="fa fa-trophy fa-5x pull-left fa-fw"></span>
                                    <h3>GPS and Augmented Reality app</h3>
                                    <h5>Android Application for the UCR campus</h5>
                                    <h4></h4>
                                    <p>Worked in a team of four to create a GPS and Augmented Reality application for the android phone. This application used the phone's built in GPS to get the users location and provide directions <!--to wherever the user wanted. It also gave information about a building on campus when the user pointed their phone to that specific building. I was responsible for the client-side of this application strictly working with the Android interface using Java. I created the user-interface and helped design &ldquo;the look&rdquo; of our application. --></p>
                                </li>

                                <li>
                                    <span class="fa fa-microphone fa-5x pull-left fa-fw"></span>
                                    <h3>Microsoft Imagine Cup Competition (Game Design)</h3>
                                    <h5>Advanced to the National Semi-Finals</h5>
                                    <h4></h4>
                                    <p>Team member of three designers to create a game we called <em>Trash Boy</em>. The competition focused on world issues and how technology could help solve these issues. Our team name is Team Inspiration, and we created <em>Trash Boy</em> to show how pollution affects the oceans and ocean life. <!--My role in this group was to program and create trash that <em>Trash Boy</em> collected. I also helped create concepts of the game that fit into the theme of the competition. We made it to the national semi-finals to compete with five other winners chosen from the United States.--></p>
                                </li>
                            </ul>
                        </div>
                    </section>
                    <!-- Recognition section end -->

                    <footer class="row footer">
                        <a href="http://vanessaannemaciel.com">Vanessa Maciel</a>
                    </footer>
                    <!--footer end-->

                </article>
                <!-- .page-info -->
            </section><!-- Single Page end -->


            <!-- Portfolio Page -->

            <section class="single-page">
                <header class="page-title">
                    <span><i class="fa fa-briefcase fa-3x"></i></span>
                    <h2>Portfolio</h2>
                    <p class="menu-desc">Some of My Works...</p>
                </header>
                <!-- .page-title -->

			<!-- Portfolio Page Contents-->
                <article class="page-info container-fluid">
                    <section class="row portfolios">
                        <div class="portfolio-container">

                            <div class="controls">
                                <button class="filter" data-filter="all">All</button>
                                <button class="filter" data-filter=".category-1">Web Design</button>
                                <button class="filter" data-filter=".category-2">App Development</button>
                                <button class="filter" data-filter=".category-3">Gaphic Design</button>
                            </div>

                            <div id="portfolio" class="portfolio-items">

                                <!--Single portfolio item -->
                                <div class="col-sm-6 col-md-4 mix category-1" data-myorder="Web Design">
                                    <figure class="effect-roxy">
                                        <img src="<?php echo get_bloginfo( 'template_directory' );?>/img/portfolio/thumb/placeholder_thumb.jpg" alt="" />
                                        <figcaption>
                                            <h2>Project <span>Title</span></h2>
                                            <p>Your Awesome Project Description</p>
                                            <a href="#portfolio1" class="open-portfolio" >Explore</a>
                                        </figcaption>
                                    </figure>
                                </div>
                                <!-- Popup content -->
                                <div id="portfolio1" class="white-popup mfp-hide">
                                    <img src="<?php echo get_bloginfo( 'template_directory' );?>/img/portfolio/placeholder.jpg" alt="Portfolio Image">
                                    <h2 class="project-title">Awesome Project Title</h2>
                                    <p class="project-desc">Lorem ipsum dolor sit amet, quo ut reque putent. Per saperet torquatos temporibus te. Nam nulla fabellas similique id, eruditi accusamus sit ex. Modo aliquid elaboraret nam id, in vel illud numquam. Evertitur consetetur voluptatum pri id, in scaevola vulputate cum. Ea vis mazim accusam, vix ea tale appareat.</p>
                                    <a class="project-url btn" href="#">See Live</a>
                                </div>
                                <!--.Single item end-->

                                <!--Single portfolio item -->
                                <div class="col-sm-6 col-md-4 mix category-2" data-myorder="App Development">
                                    <figure class="effect-roxy">
                                        <img src="<?php echo get_bloginfo( 'template_directory' );?>/img/portfolio/thumb/placeholder_thumb.jpg" alt="" />
                                        <figcaption>
                                            <h2>Project <span>Title</span></h2>
                                            <p>Your Awesome Project Description</p>
                                            <a href="#portfolio2" class="open-portfolio" >Explore</a>
                                        </figcaption>
                                    </figure>
                                </div>
                                <!-- Popup content -->
                                <div id="portfolio2" class="white-popup mfp-hide">
                                    <img src="<?php echo get_bloginfo( 'template_directory' );?>/img/portfolio/placeholder.jpg" alt="Portfolio Image">
                                    <h2 class="project-title">Awesome Project Title</h2>
                                    <p class="project-desc">Lorem ipsum dolor sit amet, quo ut reque putent. Per saperet torquatos temporibus te. Nam nulla fabellas similique id, eruditi accusamus sit ex. Modo aliquid elaboraret nam id, in vel illud numquam. Evertitur consetetur voluptatum pri id, in scaevola vulputate cum. Ea vis mazim accusam, vix ea tale appareat.</p>
                                    <a class="project-url btn" href="#">See Live</a>
                                </div>
                                <!--.Single item end-->

                                <!--Single portfolio item -->
                                <div class="col-sm-6 col-md-4 mix category-3" data-myorder="Graphic Design">
                                    <figure class="effect-roxy">
                                        <img src="<?php echo get_bloginfo( 'template_directory' );?>/img/portfolio/thumb/placeholder_thumb.jpg" alt="" />
                                        <figcaption>
                                            <h2>Project <span>Title</span></h2>
                                            <p>Your Awesome Project Description</p>
                                            <a href="#portfolio3" class="open-portfolio" >Explore</a>
                                        </figcaption>
                                    </figure>
                                </div>
                                <!-- Popup content -->
                                <div id="portfolio3" class="white-popup mfp-hide">
                                    <img src="<?php echo get_bloginfo( 'template_directory' );?>/img/portfolio/placeholder.jpg" alt="Portfolio Image">
                                    <h2 class="project-title">Awesome Project Title</h2>
                                    <p class="project-desc">Lorem ipsum dolor sit amet, quo ut reque putent. Per saperet torquatos temporibus te. Nam nulla fabellas similique id, eruditi accusamus sit ex. Modo aliquid elaboraret nam id, in vel illud numquam. Evertitur consetetur voluptatum pri id, in scaevola vulputate cum. Ea vis mazim accusam, vix ea tale appareat.</p>
                                    <a class="project-url btn" href="#">See Live</a>
                                </div>
                                <!--.Single item end-->

                                <!--Single portfolio item -->
                                <div class="col-sm-6 col-md-4 mix category-1" data-myorder="Web Design">
                                    <figure class="effect-roxy">
                                        <img src="<?php echo get_bloginfo( 'template_directory' );?>/img/portfolio/thumb/placeholder_thumb.jpg" alt="" />
                                        <figcaption>
                                            <h2>Project <span>Title</span></h2>
                                            <p>Your Awesome Project Description</p>
                                            <a href="#portfolio4" class="open-portfolio" >Explore</a>
                                        </figcaption>
                                    </figure>
                                </div>
                                <!-- Popup content -->
                                <div id="portfolio4" class="white-popup mfp-hide">
                                    <img src="<?php echo get_bloginfo( 'template_directory' );?>/img/portfolio/placeholder.jpg" alt="Portfolio Image">
                                    <h2 class="project-title">Awesome Project Title</h2>
                                    <p class="project-desc">Lorem ipsum dolor sit amet, quo ut reque putent. Per saperet torquatos temporibus te. Nam nulla fabellas similique id, eruditi accusamus sit ex. Modo aliquid elaboraret nam id, in vel illud numquam. Evertitur consetetur voluptatum pri id, in scaevola vulputate cum. Ea vis mazim accusam, vix ea tale appareat.</p>
                                    <a class="project-url btn" href="#">See Live</a>
                                </div>
                                <!--.Single item end-->

                                <!--Single portfolio item -->
                                <div class="col-sm-6 col-md-4 mix category-2" data-myorder="App Development">
                                    <figure class="effect-roxy">
                                        <img src="<?php echo get_bloginfo( 'template_directory' );?>/img/portfolio/thumb/placeholder_thumb.jpg" alt="" />
                                        <figcaption>
                                            <h2>Project <span>Title</span></h2>
                                            <p>Your Awesome Project Description</p>
                                            <a href="#portfolio5" class="open-portfolio" >Explore</a>
                                        </figcaption>
                                    </figure>
                                </div>
                                <!-- Popup content -->
                                <div id="portfolio5" class="white-popup mfp-hide">
                                    <img src="<?php echo get_bloginfo( 'template_directory' );?>/img/portfolio/placeholder.jpg" alt="Portfolio Image">
                                    <h2 class="project-title">Awesome Project Title</h2>
                                    <p class="project-desc">Lorem ipsum dolor sit amet, quo ut reque putent. Per saperet torquatos temporibus te. Nam nulla fabellas similique id, eruditi accusamus sit ex. Modo aliquid elaboraret nam id, in vel illud numquam. Evertitur consetetur voluptatum pri id, in scaevola vulputate cum. Ea vis mazim accusam, vix ea tale appareat.</p>
                                    <a class="project-url btn" href="#">See Live</a>
                                </div>
                                <!--.Single item end-->

                                <!--Single portfolio item -->
                                <div class="col-sm-6 col-md-4 mix category-3" data-myorder="Graphic Design">
                                    <figure class="effect-roxy">
                                        <img src="<?php echo get_bloginfo( 'template_directory' );?>/img/portfolio/thumb/placeholder_thumb.jpg" alt="" />
                                        <figcaption>
                                            <h2>Project <span>Title</span></h2>
                                            <p>Your Awesome Project Description</p>
                                            <a href="#portfolio6" class="open-portfolio" >Explore</a>
                                        </figcaption>
                                    </figure>
                                </div>
                                <!-- Popup content -->
                                <div id="portfolio6" class="white-popup mfp-hide">
                                    <img src="<?php echo get_bloginfo( 'template_directory' );?>/img/portfolio/placeholder.jpg" alt="Portfolio Image">
                                    <h2 class="project-title">Awesome Project Title</h2>
                                    <p class="project-desc">Lorem ipsum dolor sit amet, quo ut reque putent. Per saperet torquatos temporibus te. Nam nulla fabellas similique id, eruditi accusamus sit ex. Modo aliquid elaboraret nam id, in vel illud numquam. Evertitur consetetur voluptatum pri id, in scaevola vulputate cum. Ea vis mazim accusam, vix ea tale appareat.</p>
                                    <a class="project-url btn" href="#">See Live</a>
                                </div>
                                <!--.Single item end-->

                                <!--Single portfolio item -->
                                <div class="col-sm-6 col-md-4 mix category-1" data-myorder="Graphic Design">
                                    <figure class="effect-roxy">
                                        <img src="<?php echo get_bloginfo( 'template_directory' );?>/img/portfolio/thumb/placeholder_thumb.jpg" alt="" />
                                        <figcaption>
                                            <h2>Project <span>Title</span></h2>
                                            <p>Your Awesome Project Description</p>
                                            <a href="#portfolio7" class="open-portfolio" >Explore</a>
                                        </figcaption>
                                    </figure>
                                </div>
                                <!-- Popup content -->
                                <div id="portfolio7" class="white-popup mfp-hide">
                                    <img src="<?php echo get_bloginfo( 'template_directory' );?>/img/portfolio/placeholder.jpg" alt="Portfolio Image">
                                    <h2 class="project-title">Awesome Project Title</h2>
                                    <p class="project-desc">Lorem ipsum dolor sit amet, quo ut reque putent. Per saperet torquatos temporibus te. Nam nulla fabellas similique id, eruditi accusamus sit ex. Modo aliquid elaboraret nam id, in vel illud numquam. Evertitur consetetur voluptatum pri id, in scaevola vulputate cum. Ea vis mazim accusam, vix ea tale appareat.</p>
                                    <a class="project-url btn" href="#">See Live</a>
                                </div>
                                <!--.Single item end-->

                                <!--Single portfolio item -->
                                <div class="col-sm-6 col-md-4 mix category-2" data-myorder="Graphic Design">
                                    <figure class="effect-roxy">
                                        <img src="<?php echo get_bloginfo( 'template_directory' );?>/img/portfolio/thumb/placeholder_thumb.jpg" alt="" />
                                        <figcaption>
                                            <h2>Project <span>Title</span></h2>
                                            <p>Your Awesome Project Description</p>
                                            <a href="#portfolio8" class="open-portfolio" >Explore</a>
                                        </figcaption>
                                    </figure>
                                </div>
                                <!-- Popup content -->
                                <div id="portfolio8" class="white-popup mfp-hide">
                                    <img src="<?php echo get_bloginfo( 'template_directory' );?>/img/portfolio/placeholder.jpg" alt="Portfolio Image">
                                    <h2 class="project-title">Awesome Project Title</h2>
                                    <p class="project-desc">Lorem ipsum dolor sit amet, quo ut reque putent. Per saperet torquatos temporibus te. Nam nulla fabellas similique id, eruditi accusamus sit ex. Modo aliquid elaboraret nam id, in vel illud numquam. Evertitur consetetur voluptatum pri id, in scaevola vulputate cum. Ea vis mazim accusam, vix ea tale appareat.</p>
                                    <a class="project-url btn" href="#">See Live</a>
                                </div>
                                <!--.Single item end-->

                                <!--Single portfolio item -->
                                <div class="col-sm-6 col-md-4 mix category-1" data-myorder="Web Design">
                                    <figure class="effect-roxy">
                                        <img src="<?php echo get_bloginfo( 'template_directory' );?>/img/portfolio/thumb/placeholder_thumb.jpg" alt="" />
                                        <figcaption>
                                            <h2>Project <span>Title</span></h2>
                                            <p>Your Awesome Project Description</p>
                                            <a href="#portfolio9" class="open-portfolio" >Explore</a>
                                        </figcaption>
                                    </figure>
                                </div>
                                <!-- Popup content -->
                                <div id="portfolio9" class="white-popup mfp-hide">
                                    <img src="<?php echo get_bloginfo( 'template_directory' );?>/img/portfolio/placeholder.jpg" alt="Portfolio Image">
                                    <h2 class="project-title">Awesome Project Title</h2>
                                    <p class="project-desc">Lorem ipsum dolor sit amet, quo ut reque putent. Per saperet torquatos temporibus te. Nam nulla fabellas similique id, eruditi accusamus sit ex. Modo aliquid elaboraret nam id, in vel illud numquam. Evertitur consetetur voluptatum pri id, in scaevola vulputate cum. Ea vis mazim accusam, vix ea tale appareat.</p>
                                    <a class="project-url btn" href="#">See Live</a>
                                </div>
                                <!--.Single item end-->
                            </div>

                            <!--Load more button-->
                            <a href="#" class="btn btn-load-more">Load More</a>
                        </div>
                        <!-- .portfolio-container -->
                    </section>
                    <!-- portfolio section end-->


                    <!-- Testimonials Section
			   ================================================== -->
                    <section class="row testimonials">
                        <div class="over-div"></div>
                        <div class="sec-divider"></div>
                        <h2 class="section-title">Some of my happy clients</h2>

                        <div class="col-md-8 col-md-offset-2 text-container">
                            <div id="testimonial-slides" class="owl-carousel">
                                <div>
                                    <blockquote>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer</p>
                                        <cite> Jonathan Doe</cite>
                                    </blockquote>
                                </div>
                                <!-- Single testimonial -->

                                <div>
                                    <blockquote>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer</p>
                                        <cite> Jane Doe</cite>
                                    </blockquote>
                                </div>
                                <!-- Single testimonial -->

                                <div>
                                    <blockquote>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer</p>
                                        <cite> John Doe</cite>
                                    </blockquote>
                                </div>
                                <!-- Single testimonial -->

                            </div>
                            <!-- Owl Carousel End -->
                        </div>
                        <!-- text-container ends -->

                    </section>
                    <!-- Testimonials Section End-->


                    <section class="row fun-facts">
                        <ul>
                            <li>
                                <h3> <i class="fa fa-cutlery"></i> </h3>
                                <h5>Cook</h5>
                            </li>
                            <li>
                                <h3> <i class="fa fa-heartbeat"></i> </h3>
                                <h5>Gym Goer</h5>
                            </li>
                            <li>
                                <h3>300+</h3>
                                <h5>Coffee Cups</h5>
                            </li>
                        </ul>
                    </section>
                    <!-- Fun facts end-->

                    <footer class="row footer">
                        <a href="http://vanessaannemaciel.com">Vanessa Maciel</a>
                    </footer>
                </article>
                <!-- .page-info -->
            </section><!-- Single Page end -->


            <!-- Contact Page-->

            <section class="single-page">
                <header class="page-title">
                    <span><i class="fa fa-paper-plane fa-3x"></i></span>
                    <h2>Contact</h2>
                    <p class="menu-desc">Way to Reach Me...</p>
                </header>
                <!-- .page-title -->

			<!-- Contact Page Contents-->
                <article class="page-info container-fluid">
                    <section class="row contact">
                        <div class="over-div"></div>
                        <div class="sec-divider"></div>
                        <h2 class="section-title">Drop me a line</h2>

                        <!-- form -->
                        <section class="row">
                            <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 form-container">
                                <form action="#" id="contactForm" method="post" name="contactForm">
                                    <fieldset>
                                        <div>
                                            <input id="contactName" name="contactName" placeholder="Name_" size="35" type="text" value="">
                                        </div>

                                        <div>
                                            <input id="contactEmail" name="contactEmail" placeholder="Email_" size="35" type="email" value="">
                                        </div>

                                        <div>
                                            <textarea cols="5" id="contactMessage" name="contactMessage" placeholder="Message_" rows="5"></textarea>
                                        </div>

                                        <div>
                                            <!-- contact-warning -->

                                            <div id="message-warning">
                                                Error boy
                                            </div>
                                            <button class="submit">Send <i class="fa fa-angle-right"></i>
                                            </button>
                                            <span id="image-loader"><img alt="" src="img/svg-loaders/mail-loader.svg"></span>
                                        </div>
                                    </fieldset>
                                </form>
                                <!-- Form End -->
                                <!-- contact-success -->

                                <div id="message-success">
                                    <i class="fa fa-check"></i>Your message was sent, thank you!
                                    <br>
                                </div>
                            </div>
                       </section>
                        <!-- Form Container End -->


                        <section class="row social">
                            <div class="col-md-6 address">
                                <div class="row location">
                                    <div class="col-md-12 col-lg-10 col-lg-offset-1">
                                        <ul>
                                            <li>
                                                <img src="<?php echo get_bloginfo( 'template_directory' );?>/img/icons/location-map.svg" alt="">
                                                <h4>Culver City, Los Angeles</h4>
                                            </li>
                                            <li>
                                                <img src="<?php echo get_bloginfo( 'template_directory' );?>/img/icons/email-icon.svg" alt="">
                                                <h4>veesoria@gmail.com</h4>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <nav class="row social-profiles">
                                    <div class="col-md-12 col-lg-9 col-lg-offset-2">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-facebook fa-2x"></i></a>
                                            </li>
                                            <li><a href="#"><i class="fa fa-twitter fa-2x"></i></a>
                                            </li>
                                            <li><a href="#"><i class="fa fa-google-plus fa-2x"></i></a>
                                            </li>
                                            <li><a href="#"><i class="fa fa-github fa-2x"></i></a>
                                            </li>
                                            <li><a href="#"><i class="fa fa-dribbble fa-2x"></i></a>
                                            </li>
                                            <li><a href="#"><i class="fa fa-slideshare fa-2x"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </nav>
                            </div>
                            <div class="col-md-6 map-container">
                                <div id="map"></div>
                            </div>

                        </section>
                        <!--Social End-->
                    </section>
                    <!--Contact End-->
                    <footer class="row footer">
                        <a href="http://vanessaannemaciel.com">Vanessa Maciel</a>
                    </footer>

                </article>
                <!-- .page-info -->
            </section><!-- Single Page end -->

        <a href="#" class="page-close">Close</a>
        <a href="#" class="page-scroll">Scroll</a>
    </main>
    <!-- .page-container -->
