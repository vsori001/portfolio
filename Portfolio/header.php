<!doctype html>
<html lang="en" class="no-js">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Vanessa Anne Maciel</title>

    <script src="https://use.fontawesome.com/012ae830e6.js"></script>

    <!-- Favicon-->
	<!-- <link rel="shortcut icon" href="img/favicon.png" > -->

    <!-- CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo get_bloginfo( 'template_directory' );?>/lib/magnific-popup/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo get_bloginfo( 'template_directory' );?>/lib/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo get_bloginfo( 'template_directory' );?>/lib/owl-carousel/owl.theme.css">
    <link rel="stylesheet" href="<?php echo get_bloginfo( 'template_directory' );?>/css/style.css">
    <link rel="stylesheet" href="<?php echo get_bloginfo( 'template_directory' );?>/css/responsive.css">

    <!-- Modernizr -->
    <script src="<?php echo get_bloginfo( 'template_directory' );?>/js/modernizr.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php wp_head(); ?>
</head>

<body>
