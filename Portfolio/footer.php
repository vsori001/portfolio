<!-- JavaScripts -->
<script src="<?php echo get_bloginfo( 'template_directory' );?>/js/jquery-2.1.1.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&AMP;sensor=false"></script>
<script src="<?php echo get_bloginfo( 'template_directory' );?>/js/gmaps.js"></script>
<script src="<?php echo get_bloginfo( 'template_directory' );?>/lib/magnific-popup/jquery.magnific-popup.js"></script>
<script src="<?php echo get_bloginfo( 'template_directory' );?>/lib/mixitup/jquery.mixitup.js"></script>
<script src="<?php echo get_bloginfo( 'template_directory' );?>/lib/owl-carousel/owl.carousel.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/0.2.0/Chart.min.js"></script>
<script src="<?php echo get_bloginfo( 'template_directory' );?>/js/scripts.js"></script>

<?php wp_footer(); ?>

</body>

</html>
